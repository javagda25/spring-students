package com.javagda25.spring.students.controller;

import com.javagda25.spring.students.model.Grade;
import com.javagda25.spring.students.model.GradeSubject;
import com.javagda25.spring.students.service.GradeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/grade/")
@AllArgsConstructor
public class GradeController {

    private final GradeService gradeService;

    @GetMapping("/add/{studentId}")
    public String gradeForm(Model model, Grade grade,
                            @PathVariable(name = "studentId") Long studentId) {
        model.addAttribute("grade", grade);
        model.addAttribute("subjects", GradeSubject.values());
        model.addAttribute("studentId", studentId);

        return "grade-add";
    }

    @GetMapping("/edit/{id}")
    public String gradeForm(Model model, @PathVariable(name = "id") Long gradeId) {
        Optional<Grade> optionalGrade = gradeService.findById(gradeId);
        if (optionalGrade.isPresent()) {
            model.addAttribute("grade", optionalGrade.get());
            model.addAttribute("subjects", GradeSubject.values());
            model.addAttribute("studentId", optionalGrade.get().getStudent().getId());

            return "grade-add";
        }
        return "redirect:/grade/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long gradeId) {
        gradeService.deleteById(gradeId);
        return "redirect:/grade/list";
    }

    @PostMapping("/add")
    public String gradeAdd(Grade grade, Long studentParam) {
        gradeService.save(grade, studentParam);
        return "redirect:/grade/list";
    }

    @GetMapping("/list")
    public String list(Model model) {
        List<Grade> gradeList = gradeService.findAll();
        model.addAttribute("grades", gradeList);

        return "grade-list";
    }

}
