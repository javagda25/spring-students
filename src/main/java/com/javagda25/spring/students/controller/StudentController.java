package com.javagda25.spring.students.controller;

import com.javagda25.spring.students.model.Student;
import com.javagda25.spring.students.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping(path = "/student/")
public class StudentController {
    private final StudentService studentService;

    // Get - Wyświetla użytkownikowi formularz dodawania studenta
    @GetMapping("/add")
    public String add(Model model, Student student) {
        model.addAttribute("student", student);
        return "student-add";
    }

    // Post - jest do obsługi wypełnionego formularza. Po tym jak formularz
    // został wyświetlony użytkowikowi on wypełnia go. Następnie klikając "Prześlij"
    // użytkownik wysyła treść formularza do metody POST (NIE GET)
    @PostMapping("/add")
    public String add(Student student) {
        studentService.add(student);

        return "redirect:/student/list";
    }

    @GetMapping("/list")
    public String list(Model model) {
        List<Student> studentList = studentService.getAllStudents();

        model.addAttribute("students", studentList);

        return "student-list";
    }


    @GetMapping("/edit/{edited_student_id}")
    public String edit(Model model,
                       @PathVariable(name = "edited_student_id") Long studentId) {
        return editStudent(model, studentId);
    }

    //    2 edit z request param
    @GetMapping("/edit")
    public String editParam(Model model,
                       @RequestParam(name = "studentId") Long studentId) {
        return editStudent(model, studentId);
    }
    //    2 edit z request param
    @GetMapping("/grades/{id}")
    public String studentGrades(Model model,
                       @PathVariable(name = "id") Long studentId) {
        Optional<Student> studentOptional = studentService.findById(studentId);
        if(studentOptional.isPresent()){
            model.addAttribute("grades", studentOptional.get().getGradeList());
            return "grade-list";
        }
        return "redirect:/student/list";
    }

    private String editStudent(Model model, Long studentId) {
        Optional<Student> studentOptional = studentService.findById(studentId);
        if (studentOptional.isPresent()) {
            model.addAttribute("student", studentOptional.get());

            return "student-add";
        }
        return "redirect:/student/list";
    }


    @GetMapping("/delete/{delete_id}")
    public String delete(@PathVariable(name = "delete_id") Long id) {
        return deleteStudent(id);
    }
//    3 delete z request param

    @GetMapping("/delete")
    public String deleteParam(@RequestParam(name = "studentId") Long id) {
        return deleteStudent(id);
    }

    private String deleteStudent(Long id) {
        studentService.deleteById(id);

        return "redirect:/student/list";
    }
}
